(ns todayim-api.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [ring.middleware.json :refer [wrap-json-response]]))



(defroutes app-routes
  (GET "/login/" [] "Hello World")
  (GET "/goals.json" [] "Goals")
  (GET "/habits.json" [] "Habits")
  (route/not-found "Not Found"))


(def app
  (-> app-routes
      (wrap-json-response)
      (wrap-defaults api-defaults)))