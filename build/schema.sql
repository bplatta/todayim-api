-- https://www.postgresql.org/docs/9.6/static/runtime-config-client.html
SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

CREATE TABLE todayim_data.user (
    id          BIGSERIAL PRIMARY KEY,
    name        VARCHAR(100) NOT NULL,
    email       VARCHAR(255) NOT NULL,
    created_at  DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    password    VARCHAR(30) NOT NULL,
    CONSTRAINT  user_email_unique UNIQUE(email)
);

CREATE TABLE todayim_data.goal (
    id              BIGSERIAL PRIMARY KEY,
    user_id         INTEGER REFERENCES todayim_data.user(id) ON DELETE CASCADE,
    name            TEXT NOT NULL,
    description     TEXT DEFAULT '',
    target_date     DATE DEFAULT NULL,
    created_at      DATE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE todayim_data.plan (
    id            BIGSERIAL PRIMARY KEY,
    goal_id       INTEGER REFERENCES todayim_data.goal(id) ON DELETE CASCADE,
    name          TEXT NOT NULL,
    description   TEXT DEFAULT '',
    created_at    DATE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE todayim_data.step (
    id            BIGSERIAL PRIMARY KEY,
    plan_id       INTEGER REFERENCES todayim_data.plan(id) ON DELETE CASCADE,
    priority      INTEGER NOT NULL DEFAULT 1,
    target_date   DATE NOT NULL,
    parent        INTEGER REFERENCES todayim_data.step(id) ON DELETE CASCADE DEFAULT NULL,
    created_at    DATE NOT NULL DEFAULT CURRENT_TIMESTAMP
);