(defproject todayim-api "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.6.1"]
                 [org.postgresql/postgresql "9.4-1206-jdbc41"]
                 [honeysql "0.8.1"]
                 [ragtime "0.6.0"]
                 [ring/ring-core "1.5.0"]
                 [ring/ring-defaults "0.2.1"]
                 [ring/ring-jetty-adapter "1.5.0"]
                 [compojure "1.5.1"]
                 [ring/ring-json "0.4.0"]]
  :plugins [[lein-ring "0.10.0"]]
  :ring {:handler todayim-api.handler/app}
  :aliases {"migrate" ["run" "-m" "db.manage/migrate"]
            "rollback" ["run" "-m" "db.manage/rollback"]}
  :profiles {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]]
                        [ring/ring-mock "0.3.0"]}})
