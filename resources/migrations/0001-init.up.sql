CREATE TABLE todayim_data.user (
    id          BIGSERIAL PRIMARY KEY,
    name        VARCHAR(100) NOT NULL,
    email       VARCHAR(255) NOT NULL,
    created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    password    VARCHAR(30) NOT NULL,
    CONSTRAINT  user_email_unique UNIQUE(email)
);

CREATE TABLE todayim_data.goal (
    id              BIGSERIAL PRIMARY KEY,
    user_id         INTEGER REFERENCES todayim_data.user(id) ON DELETE CASCADE NOT NULL,
    name            TEXT NOT NULL,
    description     TEXT DEFAULT '',
    target_date     TIMESTAMP DEFAULT NULL,
    created_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE todayim_data.plan (
    id            BIGSERIAL PRIMARY KEY,
    goal_id       INTEGER REFERENCES todayim_data.goal(id) ON DELETE CASCADE NOT NULL,
    name          TEXT NOT NULL,
    description   TEXT DEFAULT '',
    created_at    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE todayim_data.plan_step (
    id            BIGSERIAL PRIMARY KEY,
    plan_id       INTEGER REFERENCES todayim_data.plan(id) ON DELETE CASCADE NOT NULL,
    priority      INTEGER NOT NULL DEFAULT 1,
    target_date   TIMESTAMP NOT NULL,
    parent        INTEGER REFERENCES todayim_data.plan_step(id) ON DELETE CASCADE DEFAULT NULL,
    created_at    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE todayim_data.api_key (
    id            BIGSERIAL PRIMARY KEY,
    user_id       INTEGER REFERENCES todayim_data.user(id) ON DELETE CASCADE NOT NULL,
    key           VARCHAR(100) NOT NULL
);