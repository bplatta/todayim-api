.PHONY: help test clean build build-db start restart stop sandbox test-unit

major_version := 0
minor_version := 1
build_number := 0
version := $(major_version).$(minor_version).$(build_number)

help:
	@echo
	@echo "TODAYIM-API makefile"
	@echo "usage: make [target] ..."
	@echo
	@egrep "^# target:" [Mm]akefile
	@echo

# target: build - Build a development environment.
build:
	docker build -f build/Dockerfile -t local/todayim-api:$(version) .

# target: build-db - Migrate database container.
build-db:
	@echo
	@echo "Building local database tests ======================="
	@echo
	mkdir -p pgdata-local

# target: start - Run local API with dependencies (postgres)
start:
	docker-compose -f deploy/docker-compose.dev.yaml up -d

# target: restart - Restart API stack
restart:
	docker-compose -f deploy/docker-compose.dev.yaml restart

# target: stop - stop current API stack
stop:
	docker-compose -f deploy/docker-compose.dev.yaml stop

# target: sandbox - jump into an api sandbox with ports open
sandbox:
	docker-compose -f deploy/docker-compose.dev.yaml run --service-ports todayim-api /bin/bash

# target: test-unit - Run the applications unit tests only
test-unit:
	@echo
	@echo "No unit tests ======= "
	@echo

# target: clean - Remove local postgres data
clean:
	@echo
	@echo "Cleaning local pg-data. Hope you meant to do this!"
	@echo
	rm -rf pgdata-local/*